module.exports = {


  friendlyName: 'List',


  description: 'List product.',


  inputs: {

  },


  exits: {
    success: {
      description: 'product was created'
    },
    
    notFound: {
        statusCode: 403,
        description: 'oooo gagal'
    }
  },


  fn: async function (inputs, exits) {

    // let currentPage = this.req.param('curPage') || 1
    // let perPage = this.req.param('perPage') || 10

    // let startFrom = (currentPage - 1) * perPage
    
    // let count = await Product.count({
    //   stok: {
    //     '>': 0
    //   }
    // })

    // let data = await Product
    //   .find({
    //     stok: {
    //       '>': 0
    //     },
    //   })
    //   .limit(perPage)
    //   .skip(startFrom)

    // let totalPages = Math.ceil(count / perPage)

    // let prev = parseInt(currentPage) - 1
    // let next = parseInt(currentPage) + 1

    // if (currentPage === 1) prev = 1
    // if (currentPage === totalPages) next = totalPages

    // // Respond with view.
    // return {
    //   datas: data,
    //   totalPage: parseInt(totalPages),
    //   totalData: count,
    //   prevPage: prev,
    //   nextPage: next,
    //   currentPage: parseInt(currentPage)
    // };
    
    let currentPage = this.req.param('curPage') || 1
    let perPage = this.req.param('perPage') || 10
    
    let startFrom = (currentPage - 1) * perPage
    
    let product = await Product.find({
        stok: {
          '>': 0
        }
      }).then((product) => {

          if (product) {

            let count = product.length
            
            let totalPages = Math.ceil(count / perPage)
            
            let prev = parseInt(currentPage) - 1
            let next = parseInt(currentPage) + 1

            if (currentPage === 1) prev = 1
            if (currentPage === totalPages) next = totalPages
            
            const payload = {
             datas: product,
             totalPage: parseInt(totalPages),
             totalData: count,
             prevPage: prev,
             nextPage: next,
             currentPage: parseInt(currentPage)
            }
            return exits.success({payload});

          } else {

            return exits.notFound({message: 'tidak ada list product'});

          }
          

          }).catch((err) => {

            return exits.notFound({error: true , message: 'error ini tuh', err});

          });


  }


};
