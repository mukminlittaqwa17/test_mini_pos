module.exports = {


  friendlyName: 'Product',


  description: 'Product something.',


  inputs: {
    
    // kode: {
    //   type: 'string',
    //   description: 'kode',
    //   required: true,
    // },
    
    nama_barang: {
      type: 'string',
      description: 'stok',
      required: true,
    },
    
    harga: {
      type: 'number',
      description: 'harga',
      required: true,
    },
    
    stok: {
      type: 'number',
      description: 'stok',
      required: true,
    },
    
    gambar: {
      type: 'string',
      description: 'gambar',
    },
    
  },


  exits: {
    success: {
      description: 'product was created'
    },
    
    notFound: {
        statusCode: 403,
        description: 'oooo gagal'
    }
    
  },


  fn: async function (inputs, exits) {
    
    // call path
    const path = require('path')
    
    let pathSaved = `assets/images/product/`
    
    this.req.file('gambar').upload({
      maxBytes: 10000000,
      dirname: path.resolve(sails.config.appPath, pathSaved)
    }, async (err, uploadedFiles) => {
      if (err) return res.serverError(err)
      
      var str = inputs.nama_barang
      var x = 001
      inputs.kode = str.slice(0, 2) + x++
      
      inputs.gambar = `${inputs.kode}/${path.basename(uploadedFiles[0].fd)}`

      const product = await Product.create(inputs).fetch().then((product) => {
      
     sails.log(product)

     return exits.success({message: 'success', product});

    }).catch((err) => {
      
      return exits.notFound({error: true ,message: 'wah gawat.... error ini tuh....', err});

    });
    
    })

  }


};
