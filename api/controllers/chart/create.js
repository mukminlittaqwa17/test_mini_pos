module.exports = {


  friendlyName: 'Create',


  description: 'Create chart.',


  inputs: {
    
    product_id: {
      type: 'string',
      required: true,
      description: 'product relasi'
    },
    
    qty: {
      type: 'number',
      description: 'jumlah',
      required: true,
    },
    
    total: {
      type: 'number',
      description: 'harga',
      required: true,
    },
    
    bayar: {
      type: 'number',
      description: 'bayar',
      required: true,
    },
    
    kembalian: {
      type: 'number',
      description: 'kembalian',
    },
    
  },


  exits: {
     success: {
        description: 'selesai'
      },

      notFound: {
        statusCode: 403,
        description: 'not found'
    }
  },


  fn: async function (inputs, exits) {
    
    inputs.kembalian = inputs.bayar - inputs.total

    const chart = await Chart.create(inputs).fetch().then((chart) => {
      
     sails.log(chart)

     return exits.success({message: 'success', chart});

    }).catch((err) => {
      
      return exits.notFound({error: true ,message: 'wah gawat.... error ini tuh....', err});

    });


  }


};
